package com.example.controller;

import com.example.dataAccess.FormPage1;
import com.example.dataAccess.FormPage2;
import com.example.dataAccess.FormPage3;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.stage.Stage;

public class FormNavigationApp extends Application{
    private Stage primaryStage;
    private Scene page1Scene, page2Scene, page3Scene;
    
    //Page1 instance
    private FormPage1 page1;

    //Page2 instance
    private FormPage2 page2;

    //Page2 instance
    private FormPage3 page3;


    @Override
    public void start(Stage primaryStage){
        this.primaryStage = primaryStage;

        //Initialize pages
        page1 = new FormPage1(this);
        page2 = new FormPage2(this);
        page3 = new FormPage3(this);

        //Create scenes for each page with specific dimensions
        page1Scene = new Scene(page1.getView(),400,300);
        page2Scene = new Scene(page2.getView(),400,300);
        page3Scene = new Scene(page3.getView(),400,300);

        //Set the initial scene to page1Scene
        primaryStage.setScene(page1Scene);
        primaryStage.setTitle("Form Navigation");
        primaryStage.show();

    }

    public void navigateToPage(){
        page2.setField2Value(page2.getField2Value());
    }

}
