package com.pojo;

public class PlayersData {
    //Attributes for the player data
    private String playerName;  //The name of the player 
    private String country;     //The country of the player
    private int age;    //The age of the player

    //Getter method for playerName
    public String getPlayerName() {
        return playerName;
    }

    //Getter method for country
    public String getCountry(){
        return country;
    }

    //Getter method for age
    public int getAge(){
        return age;
    }

    //Setter method for playerName
    public void setPlayerName(String playerName){
        this.playerName = playerName;
    }

    //Setter method for country
    public void setCountry(String country){
        this.country = country;
    }

    //Setter method for age
    public void setAge(int age){
        this.age = age;
    }
    
}