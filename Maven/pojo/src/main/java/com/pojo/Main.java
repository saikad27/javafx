package com.pojo;
import java.util.Scanner;
public class Main {
    public static void main(String[] args) {
        System.out.println("Players Info");

        PlayersData pd = new PlayersData();

        pd.setPlayerName("Shivam Dubey");
        pd.setCountry("India");
        pd.setAge(29);
        
        //Print the hard-coded data
        System.out.println("Player Name : "+pd.getPlayerName());
        System.out.println("Country Name : "+pd.getCountry());
        System.out.println("Player's Age : "+pd.getAge());

        //Create a Scanner object to take input from the user

        Scanner sc = new Scanner(System.in);

        //Prompt the user to enter the player's name
        System.out.print("Enter player's name :");
        String playerName = sc.nextLine();

        //Prompt the user to enter the country name
        System.out.print("Enter country name : ");
        String countryName = sc.nextLine();

        //Prompt the user to enter the player's age
        System.out.print("Enter Player's Age : ");
        int age = sc.nextInt();

        //Create a new PlayersData Object to store the user input

        pd = new PlayersData();

        //set the data accepted from the user 
        pd.setCountry(countryName);
        pd.setAge(age);
        pd.setPlayerName(playerName);

    }
}