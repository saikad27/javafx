package com.c2w.HomePage;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Page extends Application{

    @Override
    public void start(Stage stage){
        stage.setTitle("Maven Project");
        stage.setHeight(450);
        stage.setWidth(600);

        Label lb = new Label("Super - X 2024 By Core2web ");
        lb.setFont(new Font(20));
        lb.setScaleX(1.5);
        lb.setScaleY(1.5);
        VBox vb = new VBox();
        vb.getChildren().add(lb);
        vb.setAlignment(Pos.CENTER);
        vb.setStyle("-fx-background-color:Orange");
        Scene scene = new Scene(vb);
        stage.setScene(scene);
        
        stage.show();
    }
    
}
