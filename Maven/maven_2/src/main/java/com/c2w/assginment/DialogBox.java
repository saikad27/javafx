package com.c2w.assginment;

import javafx.application.Application;
import javafx.geometry.Insets;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.ButtonType;
import javafx.scene.control.Dialog;
import javafx.scene.control.TextField;
import javafx.scene.control.Button;
import javafx.scene.control.ButtonBar.ButtonData;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontPosture;
import javafx.scene.text.FontWeight;
import javafx.scene.text.Text;
import javafx.stage.Stage;


/**
 * DialongBox
 */
public class DialogBox extends Application{

    @Override
    public void start(Stage stage) {
        //TextField
        TextField tf = new TextField();
        //Dialog
        Dialog<String>dialog = new Dialog<String>();
        dialog.setTitle("Dialog");
        dialog.setContentText("Hello You Have Created A Dialog Box");

        ButtonType bt = new ButtonType("Ok",ButtonData.OK_DONE);
        dialog.getDialogPane().getButtonTypes().add(bt);

        Text txt = new Text("Click here --> ");
        Font font  = Font.font("verdana", FontWeight.BOLD, FontPosture.REGULAR, 12);
        txt.setFont(font);

        Button button = new Button("Show Dialog Box");
        button.setOnAction(e -> extracted(tf,dialog));
        HBox hb = new HBox(15);
        hb.setPadding(new Insets(50,150,50,60));
        hb.getChildren().addAll(txt,button);
        
        Scene scene = new Scene(new Group(hb),600,250,Color.TEAL);
        stage.setScene(scene);
        stage.show();

    }

    private void extracted(TextField tf, Dialog<String> dialog) {
        
        dialog.showAndWait();
    }

    
}