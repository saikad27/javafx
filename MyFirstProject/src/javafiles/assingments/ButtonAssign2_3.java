package javafiles.assingments;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * ButtonAssign2_1
 */
public class ButtonAssign2_3 extends Application{

    @Override
    public void start(Stage stage) {
        stage.setTitle("Core2web");
        stage.setHeight(1000);
        stage.setWidth(1200);

        Label lb = new Label();
        lb.setFont(new Font(20));
        lb.setLayoutX(430);
        lb.setLayoutY(600);
        
        TextField tf = new TextField();
        tf.getText();
        tf.setPrefSize(50,20);
        tf.setScaleX(1.5);
        tf.setScaleY(1.5);
        tf.setPrefWidth(150);
        tf.setLayoutX(520);
        tf.setLayoutY(400);
       // tf.setFont(new Font(20));

        Button bt1 = new Button("Square");
        bt1.setScaleX(1.5);
        bt1.setScaleY(1.5); 
        bt1.setStyle("-fx-background-color:Blue");
        bt1.setLayoutX(500);
        bt1.setLayoutY(480);
        bt1.setPrefWidth(200);

        Button bt2 = new Button("Square Root");
        bt2.setScaleX(1.5);
        bt2.setScaleY(1.5); 
        bt2.setStyle("-fx-background-color:Blue");
        bt2.setLayoutX(500);
        bt2.setLayoutY(550);
        bt2.setPrefWidth(200);

        bt1.setOnAction(new EventHandler<ActionEvent>() {
            //Square Button
            @Override
            public void handle(ActionEvent event) {
                int num = Integer.parseInt(tf.getText());
                int square = num*num;
                String squareStr = "Square of the given number is -->"+square;
                lb.setText(squareStr);
                System.out.println(square);

            }
            
        });
        bt2.setOnAction(new EventHandler<ActionEvent>() {
            //Square Root Button
            @Override
            public void handle(ActionEvent event) {
                int num = Integer.parseInt(tf.getText());
            
                double sqRoot = Math.sqrt(num); 
                String sqrtStr = "Square Root of the given number is -->"+sqRoot;
                
                lb.setText(sqrtStr);
                
                System.out.println(sqrtStr);

            }
            
        });
        
      
        Group gr = new Group(tf,bt1,bt2,lb);
        Scene scene = new Scene(gr);
        stage.setScene(scene);
        scene.setFill(Color.GOLD);
        stage.show();
    }
    
    
}