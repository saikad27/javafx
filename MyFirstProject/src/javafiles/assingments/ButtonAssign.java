package javafiles.assingments;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ButtonAssign extends Application{

    @Override
    public void start(Stage stage){
        //stage
        stage.setTitle("Core2Web");
        stage.setHeight(1200);
        stage.setWidth(1000);
        
        //Button
        Button bt1 = new Button("Core2Web - Java");
        bt1.setScaleX(1.5);
        bt1.setScaleY(1.5);
        bt1.setPrefSize(200,37);
        bt1.setStyle("-fx-background-color:Blue");
        bt1.setTextFill(Color.WHITE);
        
        Button bt2 = new Button("Core2Web - Super-X");
        bt2.setScaleX(1.5);
        bt2.setScaleY(1.5);
        bt2.setPrefSize(200,37);
        bt2.setTextFill(Color.WHITE);
        bt2.setStyle("-fx-background-color:Blue");
        Button bt3 = new Button("Core2Web - DSA");
        bt3.setScaleX(1.5);
        bt3.setScaleY(1.5);
        bt3.setPrefSize(200,37);
        bt3.setTextFill(Color.WHITE);
        bt3.setStyle("-fx-background-color:Blue");
       /*  bt1.setLayoutX(400);
        bt1.setLayoutY(430);*/
        bt1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                System.out.println("Java - 2024");
                bt1.setStyle("-fx-background-color:Green");
            }
            
        });
        bt2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                System.out.println("Super-X 2024");
                bt2.setStyle("-fx-background-color:Green");
            }
            
        });
        bt3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                System.out.println("DSA - 2024");
                bt3.setStyle("-fx-background-color:Green");
            }
            
        });
        //Label
        Label lb1 = new Label("core2web.in");
        lb1.setLayoutX(420);
        lb1.setLayoutY(250);
        lb1.setFont(new Font(20));
        //VBox
        VBox vb = new VBox(30);
        
        vb.getChildren().add(bt1);
        vb.getChildren().add(bt2);
        vb.getChildren().add(bt3);
        vb.setLayoutY(350);
        vb.setLayoutX(400);
        vb.setPrefWidth(250);
        Group gr = new Group(vb,lb1);
        //Scene
        Scene scene = new Scene(gr);
        scene.setFill(Color.GOLD);
        stage.setScene(scene);
        stage.show();
    }
    
}
