package javafiles.assingments;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ButtonAssign2_5 extends Application{
    int i=0;
    @Override
    public void start(Stage stage){
        stage.setTitle("c2w Creative window");
        stage.setHeight(1000);
        stage.setWidth(1200);
        String[] str = {"OLIVE","MAGENTA","TOMATO","GOLD","AQUA"};
        
        Button bt = new Button("Click me!");
        bt.setStyle("-fx-background-color:Green");
        bt.setScaleX(1.5);
        bt.setScaleY(1.5);
        VBox vb = new VBox();
        vb.setAlignment(Pos.CENTER);
        vb.getChildren().add(bt);
        bt.setOnAction(event ->{
                vb.setStyle("-fx-background-color:"+str[i]);
                i++;
                if(i==4)
                    i=0;
           
        });

        
        Scene scene = new Scene(vb);
        stage.setScene(scene);
        stage.show();
    }
    
}
