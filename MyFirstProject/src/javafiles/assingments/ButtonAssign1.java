package javafiles.assingments;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.FontWeight;
import javafx.stage.Stage;

public class ButtonAssign1 extends Application {

    @Override
    public void start(Stage stage) {
        stage.setTitle("Core2web");
        stage.setHeight(1000);
        stage.setWidth(1200);
        //Buttons
        Label lb = new Label("Core2web.in");
        lb.setLayoutY(300);
        lb.setLayoutX(530);
        lb.setFont(new Font(20));
        lb.setStyle("-fx-font-weight: bold");

        Button bt1 = new Button("OS");
        bt1.setStyle("-fx-background-color:BLUE");
        bt1.setScaleX(1.5);
        bt1.setScaleY(1.5);
        bt1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("OS");
            }
            
        });

        Button bt2 = new Button("C");
        bt2.setStyle("-fx-background-color:BLUE");
        bt2.setScaleX(1.5);
        bt2.setScaleY(1.5);
        bt2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("C");
            }
            
        });
        Button bt3 = new Button("CPP");
        bt3.setStyle("-fx-background-color:BLUE");
        bt3.setScaleX(1.5);
        bt3.setScaleY(1.5);
        bt3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("CPP");
            }
            
        });
        Button bt4 = new Button("JAVA");
        bt4.setStyle("-fx-background-color:BLUE");
        bt4.setScaleX(1.5);
        bt4.setScaleY(1.5);
        bt4.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("JAVA");
            }
            
        });
        Button bt5 = new Button("DSA");
        bt5.setStyle("-fx-background-color:BLUE");
        bt5.setScaleX(1.5);
        bt5.setScaleY(1.5);
        bt5.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("DSA");
            }
            
        });
        Button bt6 = new Button("Python");
        bt6.setStyle("-fx-background-color:BLUE");
        bt6.setScaleX(1.5);
        bt6.setScaleY(1.5);
        bt6.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("Python");
            }
            
        });
        //HBox
        HBox hb1 = new HBox(40);
        hb1.getChildren().add(bt1);
        hb1.getChildren().add(bt2);
        hb1.getChildren().add(bt3);
        
        hb1.setLayoutX(500);
        hb1.setLayoutY(490);

        HBox hb2 = new HBox(50);
        hb2.getChildren().add(bt4);
        hb2.getChildren().add(bt5);
        hb2.getChildren().add(bt6);
        
        hb2.setLayoutX(470);
        hb2.setLayoutY(550);
        
        //Group
        Group gr = new Group(lb,hb1,hb2);
        
        //Scene
        Scene scene = new Scene(gr);
        scene.setFill(Color.GOLD);
        stage.setScene(scene);

        stage.show();
    }
    
}
