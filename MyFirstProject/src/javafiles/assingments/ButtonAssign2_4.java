package javafiles.assingments;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ButtonAssign2_4 extends Application{

    @Override
    public void start(Stage stage){
        stage.setTitle("c2w");
        stage.setHeight(1000);
        stage.setWidth(1200);

        //TextField
        TextField tField = new TextField();
        tField.setScaleX(1.5);
        tField.setScaleY(1.5);
        tField.setLayoutY(100);
        tField.setLayoutX(100);
        //button
        Button bt = new Button("Check");
        bt.setPrefWidth(200);
        bt.setLayoutY(300);
        bt.setLayoutX(100);
        bt.setScaleX(1.5);
        bt.setScaleY(1.5);
        bt.setStyle("-fx-background-color: BLUE");

        //Label
        Label lb1 = new Label();
        lb1.setFont(new Font(20));
        lb1.setScaleX(1.5);
        lb1.setScaleY(1.5);

        bt.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                int num = Integer.parseInt(tField.getText());
                int temp = num; //123
                int reverse =0;
                int rem =0;
                
                while(temp!=0){ 
                    rem = temp%10;  //3-->2-->1
                    reverse = (reverse*10)+rem;   //30-->32-->321
                    temp = temp/10; //123-->12-->1-->0  
                }
                if(num==reverse)
                    lb1.setText("Palindrome");   
                else 
                    lb1.setText("Not a palindrome");
            }
            
        });
        VBox vb = new VBox(50);
        vb.getChildren().add(tField);
        vb.getChildren().add(bt);
        vb.getChildren().add(lb1);
        vb.setLayoutX(100);
        vb.setLayoutY(100);
        vb.setPrefWidth(200);
        vb.setPrefHeight(600);

        Group gr = new Group(vb);
        Scene scene = new Scene(gr);
        stage.setScene(scene);
        scene.setFill(Color.GOLD);
        stage.show();
    }
    
}