package javafiles.assingments;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ButtonAssign2 extends Application{

    @Override
    public void start(Stage stage) {
        stage.setTitle("Core2web");
        stage.setHeight(1000);
        stage.setWidth(1300);
        //Buttons
        Button bt1 = new Button("January");
        
        Button bt2 = new Button("February");
        
        Button bt3 = new Button("March");
        
        Button bt4 = new Button("April");
        
        Button bt5 = new Button("May");
        
        Button bt6 = new Button("June");
        
        Button bt7 = new Button("July");
        
        Button bt8 = new Button("August");
        
        Button bt9 = new Button("September");
        
        Button bt10 = new Button("October");
        
        Button bt11 = new Button("November");
        
        Button bt12 = new Button("December");

        bt1.setFont(new Font(20));
        bt2.setFont(new Font(20));
        bt3.setFont(new Font(20));
        bt4.setFont(new Font(20));
        bt5.setFont(new Font(20));
        bt6.setFont(new Font(20));
        bt7.setFont(new Font(20));
        bt8.setFont(new Font(20));
        bt9.setFont(new Font(20));
        bt10.setFont(new Font(20));
        bt11.setFont(new Font(20));
        bt12.setFont(new Font(20));

        bt1.setPrefWidth(300);
        bt2.setPrefWidth(300);
        bt3.setPrefWidth(300);
        bt4.setPrefWidth(300);
        bt5.setPrefWidth(300);
        bt6.setPrefWidth(300);
        bt7.setPrefWidth(300);
        bt8.setPrefWidth(300);
        bt9.setPrefWidth(300);
        bt10.setPrefWidth(300);
        bt11.setPrefWidth(300);
        bt12.setPrefWidth(300);

        bt1.setStyle("-fx-background-color:BLUE");
        bt2.setStyle("-fx-background-color:BLUE");
        bt3.setStyle("-fx-background-color:BLUE");
        bt4.setStyle("-fx-background-color:BLUE");
        bt5.setStyle("-fx-background-color:BLUE");
        bt6.setStyle("-fx-background-color:BLUE");
        bt7.setStyle("-fx-background-color:BLUE");
        bt8.setStyle("-fx-background-color:BLUE");
        bt9.setStyle("-fx-background-color:BLUE");
        bt10.setStyle("-fx-background-color:BLUE");
        bt11.setStyle("-fx-background-color:BLUE");
        bt12.setStyle("-fx-background-color:BLUE");
        bt1.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("January");
            }
            
        });
        bt2.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("February");
            }
            
        });
        bt3.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("March");
            }
            
        });
        bt4.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("April");
            }
            
        });
        bt5.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("May");
            }
            
        });
        bt6.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("June");
            }
            
        });
        bt7.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("July");
            }
            
        });
        bt8.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("August");
            }
            
        });
        bt9.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("September");
            }
            
        });
        bt10.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("October");
            }
            
        });
        bt11.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("November");
            }
            
        });
        bt12.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                System.out.println("December");
            }
            
        });
        VBox vb = new VBox(20);
        vb.getChildren().add(bt1);
        vb.getChildren().add(bt2);
        vb.getChildren().add(bt3);
        vb.getChildren().add(bt4);
        vb.getChildren().add(bt5);
        vb.getChildren().add(bt6);
        vb.getChildren().add(bt7);
        vb.getChildren().add(bt8);
        vb.getChildren().add(bt9);
        vb.getChildren().add(bt10);
        vb.getChildren().add(bt11);
        vb.getChildren().add(bt12);
        vb.setPrefWidth(300);
        vb.setLayoutY(50);
        Group gr = new Group(vb);
        Scene scene = new Scene(gr);
        stage.setScene(scene);
        scene.setFill(Color.GOLD);
        stage.show();
    }
    
}
