package javafiles.assingments;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.TextField;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

/**
 * ButtonAssign2_1
 */
public class ButtonAssign2_2 extends Application{

    @Override
    public void start(Stage stage) {
        stage.setTitle("Core2web");
        stage.setHeight(1000);
        stage.setWidth(1200);

        TextField txt = new TextField();
        txt.getText();
        txt.setPrefWidth(1200);
        txt.setLayoutY(70);
        txt.setFont(new Font(20));
        Button bt = new Button("Print Text");
        Label lb = new Label();
        lb.setFont(new Font(20));
        lb.setLayoutY(200);
        bt.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent event) {
                String str = txt.getText();
                lb.setText(str);
                System.out.println(str);

            }
            
        });
        bt.setScaleX(1.5);
        bt.setScaleY(1.5);
        bt.setLayoutY(150);
        bt.setLayoutX(0);
        bt.setPrefWidth(200);
        
        bt.setStyle("-fx-background-color:Blue");
        
        Group gr = new Group(txt,bt,lb);
        Scene scene = new Scene(gr);
        stage.setScene(scene);
        scene.setFill(Color.GOLD);
        stage.show();
    }
    
    
}