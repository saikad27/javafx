package javafiles.Home;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.image.Image;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.scene.text.Text;
import javafx.stage.Stage;

public class HomeWindow extends Application{
    Text text = null;
    @Override
    public void start(Stage myStage){
        
        //Stage
        myStage.setTitle("MyStage");
        myStage.setHeight(1000);
        myStage.setWidth(1200);
        myStage.setResizable(true);
        myStage.getIcons().add(new Image("assets/images/pngwing.com.png"));

        text = new Text(10,30,"Good evening,");
        text.setFill(Color.WHITE);
        text.setFont(new Font(30));

        Text gd = new Text(800,30,"Have a nice day");
        gd.setFont(new Font(30));
        gd.setFill(Color.YELLOW);
        //Scene
        Text text1 = new Text(10,50,"Java");
        text1.setFill(Color.YELLOW);
        text1.setFont(new Font(20));

        Text text2 = new Text(10,70,"Python");
        text2.setFill(Color.BLUE);
        text2.setFont(new Font(20));

        Text text3 = new Text(10,90,"CPP");
       // text3.setFill(Color.BLUE);
        text3.setFont(new Font(20));

        Text text4 = new Text(10,50,"web");
        text4.setFill(Color.AQUA);
        text4.setFont(new Font(20));

        Text text5 = new Text(10,70,"backend");
       // text5.setFill(Color.ORANGE);
        text5.setFont(new Font(20));

        Text text6 = new Text(10,90,"App");
        //text6.setFill(Color.BLUE);
        text6.setFont(new Font(20));

         //VBox
         VBox vb1 = new VBox(10,text1,text2,text3);
         vb1.setLayoutX(50);
         vb1.setLayoutY(50);

         VBox vb2 = new VBox(10,text4,text5,text6);
         vb2.setLayoutX(200);
         vb2.setLayoutY(50);

         //HBox
         HBox hb = new HBox(10,vb1,vb2);
         hb.setLayoutX(250);
         hb.setLayoutY(250);
         hb.setAlignment(Pos.CENTER);


        Group gr = new Group(hb,text,gd);
        Scene scene = new Scene(gr);
        scene.setFill(Color.MAROON);
        myStage.setScene(scene);
        
        
    
        myStage.show();
        
    }
}
