package javafiles.Home;

import javafx.application.Application;
import javafx.geometry.Pos;

import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;

import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MyPane extends Application{
    public void start(Stage stage){

        //stage
        stage.setTitle("MyStage");
        stage.setHeight(500);
        stage.setWidth(700);
        stage.setResizable(true);
        stage.getIcons().add(new Image("assets/images/pngwing.com.png"));
    
        //label
        Label l1 = new Label("Good Day");
        l1.setFont(new Font(30));
        l1.setAlignment(Pos.TOP_CENTER);
        

        //scene
        Scene scene = new Scene(l1);
        scene.setFill(Color.ORANGE);
        stage.setScene(scene);
        stage.show();

    }
}
