package javafiles.Home;

import javafx.application.Application;
// import javafx.geometry.Pos;
//import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.BorderPane;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class Assignment1 extends Application{

    @Override
    public void start(Stage primaryStage) {
        primaryStage.setTitle("C2W_Assignment_1");
        primaryStage.setWidth(600);
        primaryStage.setHeight(450);

        Label lb = new Label("Super - X 2024 by Core2Web");
        lb.setFont(new Font(20));
        BorderPane bp = new BorderPane();
        bp.setCenter(lb);
        
        //Group 
        //Group gr = new Group(lb);
        Scene sc = new Scene(bp,10,10,Color.ORANGE);
        //sc.setFill(Color.ORANGE);
        

        primaryStage.setScene(sc);
        primaryStage.show();
        
           
    }

    
}