package javafiles.imagepage;

import javafx.application.Application;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;


public class ImagePage extends Application{

    @Override
    public void start(Stage stage) {
        //Stage
        stage.setTitle("ImagePage");
        stage.setWidth(1800);
        stage.setHeight(1000);
        stage.setResizable(true);

        //ImagePage
        Image ig = new Image("assets/images/adam-kool-ndN00KmbJ1c.jpg");
        
        ImageView iv = new ImageView(ig);
        iv.setFitHeight(200);
        iv.setFitWidth(300);
        iv.isPreserveRatio();
        
        //Label
        Label lb = new Label("JavaFx Practical");
        lb.setFont(new Font(25));
        lb.setPrefWidth(500);
        lb.setPrefHeight(200);  
        
        
        
        //HBoxs
        HBox hb = new HBox(50,iv,lb);
        
        hb.setStyle("-fx-background-Color:SANDYBROWN");
        hb.setPrefHeight(200);
        hb.setPrefWidth(600);
        hb.setAlignment(Pos.CENTER);
       
        
        //Group
        Group gr = new Group(hb);
    
        //Scene
        Scene scene = new Scene(gr,Color.AQUA);
        stage.setScene(scene);
        stage.show();
    }
    
}
