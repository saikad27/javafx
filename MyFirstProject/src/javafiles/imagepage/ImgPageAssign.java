package javafiles.imagepage;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.HBox;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class ImgPageAssign extends Application {

    @Override
    public void start(Stage stage) {
        //stage
        stage.setTitle("ImagePage");
        stage.setWidth(1200);
        stage.setHeight(900);
        stage.setResizable(true);
        
        //Image
        Image img1 = new Image("assets/images/Java.jpg");

        Image img2 = new Image("assets/images/Python.jpg");

        Image img3 = new Image("assets/images/C.jpg");

        Image img4 = new Image("assets/images/CPP.jpg");

        Image img5 = new Image("assets/images/Spring.jpg");

        Image img6 = new Image("assets/images/React.jpg");

        Image img7 = new Image("assets/images/Flutter.png");

        Image img8 = new Image("assets/images/NodeJS.jpg");
        
        //ImageView
        ImageView iv1 = new ImageView(img1);
        iv1.setFitWidth(270);
        iv1.setFitHeight(200);
        

        ImageView iv2 = new ImageView(img2);
        iv2.setFitWidth(270);
        iv2.setFitHeight(200);

        ImageView iv3 = new ImageView(img3);
        iv3.setFitWidth(270);
        iv3.setFitHeight(200);
        //iv3.setPreserveRatio(true);

        ImageView iv4 = new ImageView(img4);
        iv4.setFitWidth(270);
        iv4.setFitHeight(200);

        ImageView iv5 = new ImageView(img5);
        iv5.setFitWidth(270);
        iv5.setFitHeight(200);

        ImageView iv6 = new ImageView(img6);
        iv6.setFitWidth(270);
        iv6.setFitHeight(200);

        ImageView iv7 = new ImageView(img7);
        iv7.setFitWidth(270);
        iv7.setFitHeight(200);

        ImageView iv8 = new ImageView(img8);
        iv8.setFitWidth(270);
        iv8.setFitHeight(200);

        //Label
        Label lb1 = new Label("Java");
        lb1.setFont(new Font(40));
        lb1.setPrefHeight(200);
        lb1.setGraphicTextGap(200);
       // lb1.setPrefWidth(400);

        Label lb2 = new Label("Python");
        lb2.setFont(new Font(40));
        lb2.setPrefHeight(200);

        Label lb3 = new Label("C");
        lb3.setFont(new Font(40));
        lb3.setPrefHeight(200);

        Label lb4 = new Label("CPP");
        lb4.setFont(new Font(40));
        lb4.setPrefHeight(200);

        Label lb5 = new Label("Spring");
        lb5.setFont(new Font(40));
        lb5.setPrefHeight(200);

        Label lb6 = new Label("React");
        lb6.setFont(new Font(40));
        lb6.setPrefHeight(200);

        Label lb7 = new Label("Flutter");
        lb7.setFont(new Font(40));
        lb7.setPrefHeight(200);

        Label lb8 = new Label("Node");
        lb8.setFont(new Font(40));
        lb8.setPrefHeight(200);

        //HBox
        HBox hb1 = new HBox(100);
        hb1.getChildren().add(iv1);
        hb1.getChildren().add(lb1);
        hb1.setPrefWidth(590);
        hb1.setPrefHeight(200);
        hb1.setStyle("-fx-background-color:AQUA");
        
        
        HBox hb2 = new HBox(100);
        hb2.getChildren().add(iv2);
        hb2.getChildren().add(lb2);
        hb2.setPrefWidth(590);
        hb2.setPrefHeight(200);
        hb2.setLayoutY(210);
        hb2.setLayoutX(0);
        hb2.setStyle("-fx-background-color:ORANGE");

        HBox hb3 = new HBox(100);
        hb3.getChildren().add(iv3);
        hb3.getChildren().add(lb3);
        hb3.setPrefWidth(590);
        hb3.setPrefHeight(200);
        hb3.setLayoutY(420);
        hb3.setLayoutX(0);
        hb3.setStyle("-fx-background-color:TEAL");

        HBox hb4 = new HBox(100);
        hb4.getChildren().add(iv4);
        hb4.getChildren().add(lb4);
        hb4.setPrefWidth(590);
        hb4.setPrefHeight(200);
        hb4.setLayoutY(630);
        hb4.setLayoutX(0);
        hb4.setStyle("-fx-background-color:SANDYBROWN");

        HBox hb5 = new HBox(100);
        hb5.getChildren().add(iv5);
        hb5.getChildren().add(lb5);
        hb5.setPrefWidth(590);
        hb5.setPrefHeight(200);
        hb5.setLayoutY(0);
        hb5.setLayoutX(600);
        hb5.setStyle("-fx-background-color:AQUA");

        HBox hb6 = new HBox(100);
        hb6.getChildren().add(iv6);
        hb6.getChildren().add(lb6);
        hb6.setPrefWidth(590);
        hb6.setPrefHeight(200);
        hb6.setLayoutY(210);
        hb6.setLayoutX(600);
        hb6.setStyle("-fx-background-color:ORANGE");
    
        HBox hb7 = new HBox(100);
        hb7.getChildren().add(iv7);
        hb7.getChildren().add(lb7);
        hb7.setPrefWidth(590);
        hb7.setPrefHeight(200);
        hb7.setLayoutY(420);
        hb7.setLayoutX(600);
        hb7.setStyle("-fx-background-color:TEAL");

        HBox hb8 = new HBox(100);
        hb8.getChildren().add(iv8);
        hb8.getChildren().add(lb8);
        hb8.setPrefWidth(590);
        hb8.setPrefHeight(200);
        hb8.setLayoutY(630);
        hb8.setLayoutX(600);
        hb8.setStyle("-fx-background-color:SANDYBROWN");

       //VBox 
        VBox vb1 = new VBox(10);
        vb1.getChildren().add(hb1);
        vb1.getChildren().add(hb2);
        vb1.getChildren().add(hb3);
        vb1.getChildren().add(hb4);

        VBox vb2 = new VBox(10);
        vb2.getChildren().add(hb5);
        vb2.getChildren().add(hb6);
        vb2.getChildren().add(hb7);
        vb2.getChildren().add(hb8);
        vb2.setLayoutX(600);

        //Group
        Group gr = new Group(vb1,vb2);
        //Scene obj only one argument allowed.If more than one argument use Group
        Scene scene = new Scene(gr);
        stage.setScene(scene);
        scene.setFill(Color.WHITESMOKE);
        stage.show();
    }
}