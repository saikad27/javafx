package javafiles.imagepage;

import javafx.application.Application;
import javafx.event.ActionEvent;
import javafx.event.EventHandler;
import javafx.geometry.Pos;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.scene.control.Button;
import javafx.scene.control.Label;
import javafx.scene.control.PasswordField;
import javafx.scene.control.TextField;
import javafx.scene.layout.Border;
import javafx.scene.layout.BorderStroke;
import javafx.scene.layout.BorderStrokeStyle;
import javafx.scene.layout.BorderWidths;
import javafx.scene.layout.CornerRadii;
import javafx.scene.layout.VBox;
import javafx.scene.paint.Color;
import javafx.scene.text.Font;
import javafx.stage.Stage;

public class MyButton extends Application{

    @Override
    public void start(Stage stage) {
        //Stage
        stage.setTitle("ButtonPage");
        stage.setHeight(1200);
        stage.setWidth(1000);
        stage.setResizable(true);
        //TextField
        TextField tf = new TextField();
        //Label
        Label lb3 = new Label();
        //PasswordField
        PasswordField pf = new PasswordField();
        //Border
        
        //Button
        Button bt = new Button("Show");
        bt.setScaleX(1.5);
        bt.setScaleY(1.5);
        Label lb = new Label();
        lb.setText("Login :");
        lb.setFont(new Font(30));
        Label lb1 = new Label();
        Label lb2= new Label();
        lb1.setText("Username");
        lb1.setFont(new Font(20));
        
        lb2.setText("Password");
        lb2.setFont(new Font(20));
       /*  bt.setLayoutX(430);
        bt.setLayoutY(400);*/
        bt.setOnAction(new EventHandler<ActionEvent>() {

            @Override
            public void handle(ActionEvent arg0) {
                String str1 = tf.getText();
                String str2 = pf.getText();
                lb3.setText("Text : "+str1+"\n Pass : "+str2);
                System.out.println(str1);
                System.out.println(str2);
            }
            
        });
        //VBox 
        VBox vb = new VBox(20);
        vb.getChildren().add(lb);
        vb.getChildren().add(lb1);
        vb.getChildren().add(tf);
        vb.getChildren().add(lb2);
        vb.getChildren().add(pf);
        vb.getChildren().add(bt);
        vb.getChildren().add(lb3);
        vb.setPrefHeight(400);
        vb.setPrefWidth(400);
        
        vb.setLayoutX(270);
        vb.setLayoutY(200);
        vb.setAlignment(Pos.CENTER);
        vb.setBorder(new Border(new BorderStroke(Color.BLACK,BorderStrokeStyle.SOLID,CornerRadii.EMPTY,BorderWidths.DEFAULT)));
        vb.setStyle("-fx-background-color:orange");
        Group gr = new Group(vb);
        //Scene
        Scene scene = new Scene(gr);
        //scene.setFill(Color.SKYBLUE);
        stage.setScene(scene);
        stage.show();
    }
    
}
