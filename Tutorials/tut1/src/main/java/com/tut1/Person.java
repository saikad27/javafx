package com.tut1;

import javafx.scene.paint.Color;

public class Person {
    private String name;
    private int age;
    private Color color; // Color property
    private String shape; // Shape property (e.g., "circle", "rectangle")

    public Person(String name, int age, Color color, String shape) {
        this.name = name;
        this.age = age;
        this.color = color;
        this.shape = shape;
    }

    public String getName() {
        return name;
    }

    public int getAge() {
        return age;
    }

    public Color getColor() {
        return color;
    }

    public String getShape() {
        return shape;
    }
}


