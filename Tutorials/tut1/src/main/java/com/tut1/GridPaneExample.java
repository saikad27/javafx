package com.tut1;

import javafx.application.Application;
import javafx.scene.Scene;
import javafx.scene.control.Label;
import javafx.scene.layout.GridPane;
import javafx.scene.paint.Color;
import javafx.scene.shape.Circle;
import javafx.scene.shape.Rectangle;
import javafx.stage.Stage;

public class GridPaneExample extends Application {

    @Override
    public void start(Stage primaryStage) {
        GridPane gridPane = new GridPane();

        // Create some Person objects
        Person person1 = new Person("Alice", 30, Color.RED, "circle");
        Person person2 = new Person("Bob", 25, Color.BLUE, "rectangle");

        // Add Person objects to the GridPane with shapes and colors
        addPersonToGridPane(gridPane, person1, 0);
        addPersonToGridPane(gridPane, person2, 1);

        Scene scene = new Scene(gridPane, 300, 200);
        primaryStage.setScene(scene);
        primaryStage.setTitle("GridPane Example");
        primaryStage.show();
    }

    // Helper method to add a Person to the GridPane
    private void addPersonToGridPane(GridPane gridPane, Person person, int row) {
        Label nameLabel = new Label(person.getName());
        Label ageLabel = new Label(String.valueOf(person.getAge()));
        
        // Create shape based on Person's shape property
        if ("circle".equalsIgnoreCase(person.getShape())) {
            Circle circle = new Circle(10, person.getColor());
            gridPane.add(circle, 2, row);
        } else if ("rectangle".equalsIgnoreCase(person.getShape())) {
            Rectangle rectangle = new Rectangle(20, 20, person.getColor());
            gridPane.add(rectangle, 2, row);
        }

        gridPane.add(nameLabel, 0, row);
        gridPane.add(ageLabel, 1, row);
    }

    
}




