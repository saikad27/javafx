package group.modules;

import javafx.application.Application;
import javafx.scene.Group;
import javafx.scene.Scene;
import javafx.stage.Stage;
import javafx.scene.image.Image;
import javafx.scene.image.ImageView;
import javafx.scene.layout.VBox;
public class MavenImage extends Application{

    @Override
    public void start(Stage stage) throws Exception {
        stage.setTitle("Sample");
        stage.setHeight(1000);
        stage.setWidth(1200);

        Image img1 = new Image("src/main/resources/NodeJs.jpg");
        Image img2 = new Image("src/main/resources/NodeJs.jpg");
        ImageView iv1 = new ImageView();
        iv1.setImage(img1);
        ImageView iv2 = new ImageView();
        iv2.setImage(img2);     
        VBox vb = new VBox();
        vb.getChildren().addAll(iv1,iv2);
        Group group = new Group(vb);
        Scene scene = new Scene(group);
        stage.setScene(scene);
        stage.show();
    }
    
}
